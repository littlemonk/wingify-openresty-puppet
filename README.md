#  openresty

----

## Usage 

1. Clone this module into your puppet modulepath
2. Include class `openresty` into your node definition. 
```
node 'web-1.example.com' {
   include openresty
}
```