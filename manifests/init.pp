# == Class: openresty
#
# Install openresty app and start a Hello World web service
#
# === Parameters
# 
# None

# === Variables
#
#
# [version]
#   openresty version to be installed - defaults to 1.11.2.2
# 
# [home_dir]
#    directory where openresty will be downloaded, un-compressed and installed
#    Also the home path of sample openresty web app
# 
# [packages]
#    required packages to build openresty
#
# [remove_array]
#    array of openresty mouldes not to compile 
#
# [add_array]
#    array of openresty modules to compile
# 
#
class openresty(
	$version = '1.11.2.2',
	$home_dir = '/home/oni',
	$packages = [
			'libreadline-dev',
			'libncurses5-dev',
			'libpcre3-dev',
			'libpcre3',
    			'libssl-dev',
			'perl',
			'make',
			'build-essential',
			'curl'
		],
	$remove_array = [
			'ngx_devel_kit_module',
			'http_echo_module',
			'http_xss_module',
			'http_coolkit_module',
			'http_srcache_module',
			'http_lua_module',
			'http_lua_upstream_module',
			'http_headers_more_module',
			'http_memc_module',
			'http_redis2_module',
			'http_redis_module',
			'http_rds_json_module',
			'http_rds_csv_module',
			],
	$add_array = [
		'luajit',
		'pcre-jit'
		]
	) {
	
	package {
		$packages: 
		ensure => present
	}

	exec {
		"download-openresty-source":
		cwd => "${home_dir}",
		path => '/sbin:/bin:/usr/bin',
		command => "wget https://openresty.org/download/openresty-${version}.tar.gz",
		creates => "${home_dir}/openresty-${version}.tar.gz"
	}

	exec {
		"untar-openresty-source":
		require => Exec['download-openresty-source'],
		cwd => "${home_dir}",
		path => '/sbin:/bin:/usr/bin',
		command => "tar -zxvf openresty-${version}.tar.gz",
		creates => "${home_dir}/openresty-{$version}"
	}

	$configure_remove_params = join($remove_array, ' --without-')

	$configure_add_params = join($add_array, ' --with-')

	$configure_statement = "/configure  --without-${configure_remove_params} --with-pcre --with-${configure_add_params}"


	exec {
		"configure-openresty":
		require => Exec['untar-openresty-source'],
		cwd => "${home_dir}/openresty-${version}",
		path => '/sbin:/bin:/usr/bin',
		command => "${home_dir}/openresty-${version}${configure_statement}",
	}
	
	exec {
		"install-openresty":
		require => Exec['configure-openresty'],
		cwd => "${home_dir}/openresty-${version}",
		path => '/sbin:/bin:/usr/bin',
		command => "make -j4 && make install && touch /usr/local/openresty/version-${version}.txt",
		user => 'root'
	}

	file {
		"${home_dir}/openresty-app":
		ensure => directory
	}

	file {
		"${home_dir}/openresty-app/conf":
		ensure => directory
	}
	
	file {
		"${home_dir}/openresty-app/logs":
		ensure => directory
	}

	file {
		"${home_dir}/openresty-app/conf/nginx.conf":
		ensure => file,
		content => file('openresty/nginx.conf') 
	}
	
	file {
		"${home_dir}/openresty-app/index.html":
		ensure => file,
		content => file('openresty/index.html') 
	}

	exec {
		"Start nginx":
	     	require => Exec['install-openresty'],
		cwd => "${home_dir}/openresty-app",
		path => '/sbin:/bin:/usr/bin:/usr/local/openresty/nginx/sbin',
		command => "nginx -p `pwd`/ -c conf/nginx.conf",
		logoutput => true
	}
}
